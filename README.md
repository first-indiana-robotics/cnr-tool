# Consent & Release Form Tool

Collecting digital forms in a sane fashion

Powered by:

- [Sinatra](https://sinatrarb.com)
- [MongoDB](https://mongodb.com) & [Mongoid](https://github.com/mongodb/mongoid)
- [Pony](https://github.com/benprew/pony) with [mailcatcher](https://mailcatcher.me/) for testing
- [Signature Pad](https://github.com/szimek/signature_pad)

## Getting Started

- Make sure you've got Ruby >~ 3.1.0 installed
- Make a `.env` file or set your environment variables appropriately - see `dotenv.env` for a template / example.
- `bundle install`
- `docker-compose start`
- `rackup`