document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('addTeam').addEventListener('click', (e) => {
    console.log('ok')
    e.preventDefault()
    Array.from(document.getElementById('affiliationPicker').selectedOptions).forEach( (opt) => {
      document.getElementById('teamList').innerHTML += `<input type="hidden" name="teams[]" value="${opt.value}" data-team="${opt.value}"></input>`
      document.getElementById('teamList').innerHTML += `<span class="badge ${opt.value.split('-')[0].toLocaleLowerCase()}" data-team="${opt.value}">${opt.innerText} <button class="remove" data-team="${opt.value}"><i data-team="${opt.value}" class="fa-solid fa-xmark"></i></span>`
      opt.selected = false
    })
    updateTeams()
    setFilter('')
    document.getElementById('teamfilter').value = ''
    document.getElementById('teamfilter').dispatchEvent('keyup')
    Array.from(document.getElementsByClassName('remove')).forEach( r => {
      r.addEventListener('click', f => {
        f.preventDefault()
        console.log(f)
        document.querySelectorAll(`[data-team=${f.path[1].dataset.team}]`).forEach( e => e.remove() )
      })
    })
  })

  Array.from(document.getElementsByClassName('remove')).forEach( r => {
    r.addEventListener('click', f => {
      f.preventDefault()
      document.querySelectorAll(`[data-team=${f.path[1].dataset.team}]`).forEach( e => e.remove() )
      updateTeams()
    })
  })

  function updateTeams() {
    let form = document.querySelector('.teamPicker')
    data = new FormData(form)
    payload = Object.fromEntries(data.entries())
    payload.teams = data.getAll('teams[]')
    if(payload.teams.length == 0) {
      alert("You don't have any team affiliations selected!")
      return;
    }

    fetch(window.location.pathname, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }).then( (res) => {      
    }).then( res => {
    })
  }

  document.getElementById('teamfilter').addEventListener('keyup', e => {
    setFilter(e.target.value)
  })

  function setFilter(tx) {
    document.querySelectorAll('#affiliationPicker option').forEach( opt => {
      opt.style.display = opt.value.includes(tx) ? 'block' : 'none'
    })
  }

  if(document.querySelector('canvas.parentSignature')) {
    const parentSignaturePad = new SignaturePad(document.querySelector('canvas.parentSignature'));
    parentSignaturePad.fromDataURL(document.querySelector('canvas.parentSignature').dataset.signature)
  }

  const participantSignaturePad = new SignaturePad(document.querySelector('canvas.participantSignature'));
  participantSignaturePad.fromDataURL(document.querySelector('canvas.participantSignature').dataset.signature)

  
})