document.addEventListener('DOMContentLoaded', function() {
  const parentSignaturePad = new SignaturePad(document.querySelector('canvas.parentSignature'));
  parentSignaturePad.addEventListener('endStroke', () => {
    document.getElementById('parentSignature').value = parentSignaturePad.toDataURL('image/png')
  })
  const participantSignaturePad = new SignaturePad(document.querySelector('canvas.participantSignature'));
  participantSignaturePad.addEventListener('endStroke', () => {
    document.getElementById('participantSignature').value = participantSignaturePad.toDataURL('image/png')
  })

  document.getElementById('participantSignatureDate').valueAsDate = new Date();
  document.getElementById('parentSignatureDate').valueAsDate = new Date();

  document.getElementById('addTeam').addEventListener('click', (e) => {
    e.preventDefault()
    Array.from(document.getElementById('affiliationPicker').selectedOptions).forEach( (opt) => {
      document.getElementById('teamList').innerHTML += `<input type="hidden" name="teams[]" value="${opt.value}" data-team="${opt.value}"></input>`
      document.getElementById('teamList').innerHTML += `<span class="badge ${opt.value.split('-')[0].toLocaleLowerCase()}" data-team="${opt.value}">${opt.innerText} <button class="remove" data-team="${opt.value}"><i data-team="${opt.value}" class="fa-solid fa-xmark"></i></span>`
      opt.selected = false
    })
    document.getElementById('teamfilter').value = ''
    setFilter('')
    Array.from(document.getElementsByClassName('remove')).forEach( r => {
      r.addEventListener('click', f => {
        f.preventDefault()
        document.querySelectorAll(`[data-team=${f.path[1].dataset.team}]`).forEach( e => e.remove() )
      })
    })
  })

  document.getElementById('teamfilter').addEventListener('keyup', e => {
    setFilter(e.target.value)
  })

  function setFilter(tx) {
    document.querySelectorAll('#affiliationPicker option').forEach( opt => {
      opt.style.display = opt.value.includes(tx) ? 'block' : 'none'
    })
  }

  document.querySelector('[data-signatory="self"]').addEventListener('click', (e) => {
    document.querySelectorAll('.participantInfo label').forEach( l => {
      l.innerHTML = l.innerHTML.replace('Participant', 'Your')
    })
    document.querySelector('.parentName').style.display = 'none'
    document.querySelector('.parentSignature').style.display = 'none'
    document.querySelector('.formDetails').style.display = 'block'
  })

  document.querySelector('[data-signatory="minor"]').addEventListener('click', (e) => {
    document.querySelectorAll('.participantInfo label').forEach( l => {
      l.innerHTML = l.innerHTML.replace('Your', 'Participant')
    })
    document.querySelector('.parentName').style.display = 'flex';
    document.querySelector('.parentSignature').style.display = 'flex'
    document.querySelector('.formDetails').style.display = 'block';
  })

  document.querySelector('[data-submit]').addEventListener('click', (e) => {
    e.preventDefault()
    form = document.querySelector('.formDetails')
    data = new FormData(form)
    payload = Object.fromEntries(data.entries())
    payload.teams = data.getAll('teams[]')
    delete payload.programPicker
    delete payload.teamPicker
    if(payload.teams.length == 0) {
      alert("You don't have any team affiliations selected!")
      return;
    }

    console.log(payload)

    fetch(window.location.pathname, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(payload)
    }).then( (res) => {
      window.location = '/'
      
    }).then( res => {
    })
  })
})