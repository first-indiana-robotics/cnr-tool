require 'dotenv/load'
require 'securerandom'
require 'sinatra/base'
require 'drb/drb'
require 'mongoid'
require 'http'

Mongoid.load!(File.join(File.dirname(__FILE__), 'mongoid.yml'))

Dir[File.join(__dir__, "/lib/**/*.rb")].each do |file|
  require file
end