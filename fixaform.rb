def fixaform
  prompt = TTY::Prompt.new
  id = ''
  while id != 'exit'
    id = prompt.ask('Submission ID: ')
    s = Submission.find(id)
    if s
      puts "Got submission"
      puts "  #{s.payload['participantName']} #{s.payload['participantGrade']}"
      puts "  #{s.payload['teams']}"
      s.payload['teams'] = prompt.ask('Team ID(s): ').split(',').map(&:strip)
      s.save
    else
      puts "Failed to find that submission"
    end
  end
end