module FontAwesomeHelper
  def font_awesome_kit_url
    ENV['FONT_AWESOME_URL'] || 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js'
  end

  def icon(icon, variant="light", *options)
    classes = [
      'fa-fw',
      "fa-#{variant}",
      "fa-#{icon}",
      *options
    ].join(' ')
    %Q{<i class="#{classes}"></i>}.html_safe
  end
end