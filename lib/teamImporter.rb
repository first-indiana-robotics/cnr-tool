require 'digest'
require 'csv'

class TeamImporter
  attr_reader :files

  def initialize
    
  end

  def pg(program)
    case program
    when 'FLL Challenge'
      'FLLc'
    when 'FLL Explore'
      'FLLe'
    else
      program
    end
  end

  def perform(data)
    im = Import.create(started_at: Time.now, log: '')
    log = []
    buckets = {}
    Team.all.each do |team|
      buckets[team.program] ||= []
      buckets[team.program] << team.number
    end
    begin
      CSV.new(data, headers: true).each do |row|
        t = Team.find_or_create_by(program: pg(row['Program']), number: row['Team Number'].to_i)
        t.name = row['Team Nickname']
        t.contacts = [
          row['LC1 Email']&.downcase || nil,
          row['LC2 Email']&.downcase || nil
        ]
        q = buckets[t.program].delete(t.number).nil? ? 'NEW' : 'OLD'
        if t.save
          log << [t.program, t.number, t.name, q, 'OK'].join(" | ")
        else
          log << [t.program, t.number, t.name, q, 'ERR', t.errors.map(&:message).join(',')].join(" | ")
        end
      end
      buckets.each do |prg, tms|
        tms.each do |t|
          team = Team.find_by(program: prg, number: t)
          log << [team.program, team.number, team.name, 'XXX', 'XXX'].join(" | ")
        end
      end
    rescue #StandardError > ex
      log << '!EXCEPTION THROWN!'
      log << $!.message
      $!.backtrace.each do |b|
        log << b
      end
    ensure
      im.update(log: log.join("\n"), finished_at: Time.now)
    end
  end
end
