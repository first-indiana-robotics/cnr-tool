require 'digest'
require 'yaml'

class FormImporter
  attr_reader :files

  def initialize
    @files = Dir['./forms/*.md']
  end

  def inspect(file)
    f = File.read(file).split('---').last.strip
    frontmatter = YAML.load_file(file)
    ['name', 'slug'].each do |k|
      raise "Missing #{k} in frontmatter for #{file.split('/').last}" unless frontmatter[k]
    end

    frontmatter['digest'] = Digest::MD5.hexdigest f
    frontmatter['content'] = f
    frontmatter
  end

  def actions

  end

  def perform
    @files.each do |f|
      import(f)
    end
  end

  def import(file)
    data = inspect(file)

    forms = Form.where(slug: data['slug'])

    return if forms.last.present? && forms.order('version ASC').last.digest == data['digest']

    data['version'] = if forms.empty?
      1
    else
      forms.count + 1
    end

    Form.create(data)
  end
end