class Submission
  include Mongoid::Document

  field :form_id, type: String

  field :payload, type: Hash

  index '$**': 'text'
  index 'payload.contactEmail' => 1
  index 'payload.zip' => 1
  index 'payload.participantSignatureDate' => 1
  index 'payload.teams' => 1
  index 'form_id' => 1
end