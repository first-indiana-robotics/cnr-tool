class Import
  include Mongoid::Document

  field :started_at, type: Time
  field :finished_at, type: Time
  field :log, type: String

  index 'started_at' => 1
  index 'finished_at' => 1
end