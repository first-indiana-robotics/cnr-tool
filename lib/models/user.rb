class User
  include Mongoid::Document

  field :email, type: String
  field :super_user, type: Boolean, default: false
  field :login_token, type: String, default: ''

  validates :email, uniqueness: true
end