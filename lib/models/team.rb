class Team
  include Mongoid::Document

  field :program, type: String
  field :number, type: Integer
  field :name, type: String
  field :contacts, type: Array, default: []

  index 'program' => 1, 'number' => 1

  def submissions
    Submission.where('payload.teams' => "#{program}-#{number}")
  end
end