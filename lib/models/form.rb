class Form
  include Mongoid::Document

  field :slug, type: String
  field :name, type: String
  field :description, type: String
  field :content, type: String
  field :digest, type: String
  field :version, type: Integer

  def display
    "#{name} v#{version}"
  end

end