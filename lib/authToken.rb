require 'securerandom'
require 'jwt'

module AuthToken
  TOKEN_SECRET = ENV.fetch('TOKEN_SECRET') { SecureRandom.hex(64) }

  def self.generate(id)
    payload = {
      id: id,
      exp: Time.now.to_i + 30 * 60 * 60, # 15 minutes
      nbf: Time.now.to_i - 10 * 60 * 60,
      iat: Time.now.to_i,
      iss: 'FINCNR'
    }
    JWT.encode payload, TOKEN_SECRET, 'HS256'
  end

  def self.validate(token)
    begin
      payload = JWT.decode token, TOKEN_SECRET, true, { algorithm: 'HS256', nbf_leeway: 3600, exp_leeway: 3600, iss: 'FINCNR', verify_iss: true, required_claims: ['exp', 'nbf', 'iat', 'iss'] }
      payload[0]['id']
    rescue
      nil
    end
  end
end