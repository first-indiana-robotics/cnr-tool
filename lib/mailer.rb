require 'pony'

class Mailer
  def initialize
    @options = {
      :address              => ENV['MAIL_SERVER'],
      :port                 => ENV['MAIL_PORT'],
      :enable_starttls_auto => true,
      :user_name            => ENV['MAIL_USER'],
      :password             => ENV['MAIL_PASSWORD'],
      :authentication       => :plain, # :plain, :login, :cram_md5, no auth by default
      :domain               => ENV['MAIL_DOMAIN'] # the HELO domain provided by the client to the server
    }
  end

  def send(to, subject, body, html_body=nil)
    Pony.mail to: to, from: ENV['MAIL_FROM'], subject: subject, html_body: html_body || body, body: body, via: :smtp, via_options: @options
  end
end