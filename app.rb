require_relative 'core.rb'
require 'pry'

class CNRTool < Sinatra::Base
  enable :sessions
  set :session_secret, ENV.fetch('SESSION_SECRET') { SecureRandom.hex(64) }
  helpers FontAwesomeHelper

  configure do
    FormImporter.new.perform

    set mailer: Mailer.new
  end

  before do
    @current_user = if session['user_id']
      begin
        User.find_by(id: session['user_id'])
      rescue
        nil
      end
    else
      nil
    end
    @flash = []
  end

  error do
    "Sorry, we ran into a problem - " + env['sinatra.error'].message
  end

  get '/' do
    @new_forms = Form.pluck(:slug).uniq.map do |slug|
      Form.where(slug: slug).order('version ASC').last
    end
    @forms = Form
    @submissions = if @current_user
                     Submission.where('payload.contactEmail' => @current_user.email.downcase).tap { |ss| @forms = @forms.where(id: { '$in' => ss.map(&:form_id) }) }
    else
      []
    end

    @contact_for = if @current_user
      Team.where(contacts: @current_user.email)
    else
      []
    end

    haml :index, layout_engine: :erb
  end

  get '/login' do
    token = AuthToken.validate(params[:token])
    user = nil
    begin
      user = User.find_by(login_token: token)
      session['user_id'] = user.id
      redirect '/'
    rescue
      user = nil
      @flash << { type: 'danger', message: 'Failed to validate login token - please try to log in again. Note that those are only good once and only for 30 minutes.'}
      haml :loginerror, layout_engine: :erb
    end
    
  end

  post '/login' do
    user = User.find_or_create_by(email: params[:email])
    token = SecureRandom.hex(12)
    user.update(login_token: token)
    authToken = AuthToken.generate(token)
    settings.mailer.send(params[:email], 'Log In to FIN C&R', "Here's the link to log in to the #{ENV['ORGANIZATION']} Consent & Release Form System: #{request.url}?token=#{authToken}", %Q{<a href="#{request.url}?token=#{authToken}">Click here to log in to the #{ENV['ORGANIZATION']} Consent & Release Form System</a>})
    redirect '/'
  end

  delete '/login' do
    session.delete 'user_id'
    redirect '/'
  end

  get '/forms/:slug/:version' do
    @form = Form.find_by(slug: params[:slug], version: params[:version])
    haml :form, layout_engine: :erb
  end

  post '/forms/:slug/:version' do
    @form = Form.find_by(slug: params[:slug], version: params[:version])
    request.body.rewind
    @payload = JSON.parse(request.body.read)
    @payload['contactPhone'].gsub!(/[^0-9]/, '')
    @payload['contactEmail'] = @payload['contactEmail'].downcase
    Submission.create(form_id: @form.id, payload: @payload)
    "OK"
  end

  get '/forms/teamsearch' do
    
  end

  get '/submissions/:id' do
    @submission = Submission.find(params[:id])
    return [403, "You're not supposed to be here"] unless @current_user.super_user || @current_user.email == @submission.payload['contactEmail']
    @form = Form.find(@submission.form_id)
    @payload = @submission.payload
    haml :print, layout_engine: :erb
  end

  post '/submissions/:id' do
    @submission = Submission.find(params[:id])
    return [403, "You're not supposed to be here"] unless @current_user.super_user || @current_user.email == @submission.payload['contactEmail']
    request.body.rewind
    @payload = JSON.parse(request.body.read)
    @submission.set('payload.teams' => @payload['teams'])
    "OK"
  end

  get '/search' do
    return [403, "You're not supposed to be here"] unless @current_user.super_user
    haml :search, layout_engine: :erb
  end

  post '/search' do
    return [403, "You're not supposed to be here"] unless @current_user.super_user
    @forms = Form.where(slug: params[:form]).map(&:id)
    @results = Submission.where(form_id: { '$in' => @forms })
    if !params[:zip].blank?
      @results = @results.where('payload.zip': params[:zip])
    end
    if !params[:team].blank?
      @results = @results.where('payload.teams': params[:team])
    end
    if !params[:date].blank?
      @results = @results.where('payload.participantSignatureDate': params[:date])
    end
    if !params[:phone].blank?
      @results = @results.where('payload.contactPhone': params[:phone].gsub(/[^0-9]/, ''))
    end
    if !params[:email].blank?
      @results = @results.where('payload.contactEmail': params[:email])
    end
    # if !params[:text].blank?
    #   @results = @results.where('$text' => { '$search' => params[:text]})
    # end

    haml :search, layout_engine: :erb
  end

  get '/superusers' do
    return [403, "You're not supposed to be here"] unless @current_user.super_user
    @users = User.where(super_user: true)
    haml :superusers, layout_engine: :erb
  end

  post '/superusers' do
    return [403, "You're not supposed to be here"] unless @current_user.super_user
    User.find_or_create_by(email: params[:email]).update(super_user: true)
    @users = User.where(super_user: true)
    haml :superusers, layout_engine: :erb
  end

  delete '/superusers' do
    return [403, "You're not supposed to be here"] unless @current_user.super_user
    User.find(params[:id]).update(super_user: false)
    @users = User.where(super_user: true)
    haml :superusers, layout_engine: :erb
  end

  get '/import' do
    @msgs = []
    @import = Import.order('started_at' => 1).last
    haml :import, layout_engine: :erb
  end

  post '/import' do
    @msgs = []
    if(Import.where(started_at: { '$ne' => nil}, finished_at: nil).present?)
      @msgs << 'You cannot start another import while one in in process.'
    elsif params[:text]
      TeamImporter.new.perform(params[:text])
    else
      tempfile = params[:file][:tempfile] 
      filename = params[:file][:filename]
      #Thread.new do
        data = File.read(tempfile).encode('UTF-8', :invalid => :replace).gsub("\u0000",'')
        TeamImporter.new.perform(data)
      #end
    end
    @import = Import.order('started_at' => 1).last
    haml :import, layout_engine: :erb
  end

  get '/teams' do
    return [403, "You're not supposed to be here"] unless @current_user.super_user
    @teams = Team.order(program: 1, number: 1)
    haml :teams, layout_engine: :erb
  end

  get '/teams/:program/:number' do
    @team = Team.where(program: params[:program], number: params[:number]).first
    return [403, "You're not supposed to be here"] unless @team && (@current_user.super_user || @team.contacts.include?(@current_user.email))
    haml :team, layout_engine: :erb
  end

  post '/teams/:program/:number/promote' do
    @team = Team.find_by(program: params[:program], number: params[:number])
    return [403, "You're not supposed to be here"] unless @current_user.super_user || @team.contacts.include?(@current_user.email)
    @team.contacts << params[:email]
    @team.save
    redirect "/teams/#{@team.program}/#{@team.number}"
  end

  post '/teams/:program/:number/demote' do
    @team = Team.find_by(program: params[:program], number: params[:number])
    return [403, "You're not supposed to be here"] unless @current_user.super_user || @team.contacts.include?(@current_user.email)
    @team.contacts.delete(params[:email])
    @team.save
    redirect "/teams/#{@team.program}/#{@team.number}"
  end

  post '/teams/:program/:number/delete' do
    @team = Team.find_by(program: params[:program], number: params[:number])
    return [403, "You're not supposed to be here"] unless @current_user.super_user || @team.contacts.include?(@current_user.email)
    @team.submissions.each(&:delete)
    @team.delete
    redirect "/teams"
  end
end
