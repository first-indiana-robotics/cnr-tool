class ScrapeTeams
  def perform
    teams = [1]
    checklist = Team.all.map { |t| "#{t.program}-#{t.number}"}
    start = 0
    query = scraper_query

    ctx = OpenSSL::SSL::SSLContext.new
    ctx.verify_mode = OpenSSL::SSL::VERIFY_NONE

    while teams.count > 0
      url = "https://es02.firstinspires.org/teams/_search?size=20&from=#{start}&source_content_type=application/json&source=#{query}"
      body = HTTP.get(url, :ssl_context => ctx)
      teams = JSON.parse(body)['hits']['hits'].map{ |e| e['_source']}
      teams.each do |team|
        number = team['team_number_yearly']
        program = translate_team(team['team_type'].downcase)
        name = team['team_nickname']

        Team.find_or_create_by(program: program, number: number).update(name: name)
        checklist.delete("#{program}-#{number}")
        puts "Adding #{program} #{number}"
      end
      start += 20
    end

    checklist.each do |team|
      program, number = team.split('-')
      # Team.find_by(program: program, number: number).delete
      puts "Killing #{team}"
    end
  end

  private

  def translate_team(code)
    {
      'frc' => 'FRC',
      'ftc' => 'FTC',
      'fll' => 'FLLc',
      'jfll' => 'FLLe'
    }[code] || binding.pry
  end

  def scraper_query
    # this will need updated every year, go steal the values from the FIRST HQ team/event search page
    seasons = [299, 297, 301, 295]
    {
      "query": {
        "bool": {
          "must": [
            {
              "bool": {
                "should": [
                  {
                    "match": {
                      "team_type": "JFLL"
                    }
                  },
                  {
                    "match": {
                      "team_type": "FLL"
                    }
                  },
                  {
                    "match": {
                      "team_type": "FTC"
                    }
                  },
                  {
                    "match": {
                      "team_type": "FRC"
                    }
                  }
                ]
              }
            },
            {
              "bool": {
                "should": seasons.map do |season|
                  {
                    "match": {
                      "fk_program_seasons": season.to_s
                    }
                  }
                end
              }
            },
            {
              "match": {
                "team_country": "USA"
              }
            },
            {
              "match": {
                "team_stateprov": "IN"
              }
            }
          ]
        }
      },
      "sort": "team_number_yearly"
    }.to_json
  end

end